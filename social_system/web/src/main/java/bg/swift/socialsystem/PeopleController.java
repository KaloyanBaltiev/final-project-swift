package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.SecondaryEducation;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.YearMonth;
import java.util.*;

/**
 * The controller.Controls two views: people.html and personDetails.html  Search all the records form db by first
 * or last name. Fees all the personal info. Allows POST requests for adding new social insurance records. Calculates
 * social security aid eligibility and amount. Sorts all the records by date.
 */

@Controller
public class PeopleController {
    private static final int MONTHS_BACK_TO_CUT_OFF = 3;
    private static final int MONTHS_BACK_TO_COUNT = 27;
    private static final int MONTHS_TO_DIVIDE_TO = 24;
    private final PersonRepository personRepository;

    public PeopleController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    //Db search engine
    @GetMapping("/people")
    String getPeople(Model model,
                     @RequestParam(value = "first_name", required = false) String firstName,
                     @RequestParam(value = "last_name", required = false) String lastName
    ) {
        Iterable<Person> filteredPeople;

        if (firstName == null || lastName == null || (firstName.isEmpty() && lastName.isEmpty())) {
            filteredPeople = personRepository.findAll();
        } else if (!firstName.isEmpty()) {
            filteredPeople = personRepository.findByFirstNameIgnoreCase(firstName);
        } else if (!lastName.isEmpty()) {
            filteredPeople = personRepository.findByLastNameIgnoreCase(lastName);
        } else {
            filteredPeople = personRepository.findByLastNameAndFirstNameAllIgnoreCase(lastName, firstName);
        }
        model.addAttribute("people", filteredPeople);
        return "people";
    }

    //Add all the personal info to the view, calculate social aid
    @GetMapping("/people/{id}")
    public String getPersonById(
            @PathVariable("id") Integer id,
            Model model
    ) {
        Optional<Person> maybePerson = personRepository.findById(id);

        if (!maybePerson.isPresent()){
            return "people";
        }

        Person person = maybePerson.get();
        initializeModel(person, model);

        //if the person is eligible, calculate the aid
        boolean isEligible = checkPersonEligibility(person);
        if (isEligible) {
            double aidAmount = calcAidAmount(person);
            String aid = String.format("%.2f", aidAmount);
            model.addAttribute("social_aid", aid);
        } else {
            model.addAttribute("social_aid", "Not Eligible");
        }
        return "personDetails";
    }

    @PostMapping("/people/{id}")
    public String addNewInsuranceRecord(
            @PathVariable("id") Integer id,
            @ModelAttribute SocialInsuranceRecord insuranceRecord,
            Model model
    ) {
        //return to homepage if person's id is not valid
        if (!(personRepository.findById(id).isPresent())){
            return "people";
        }
        Person person = personRepository.findById(id).get();

        //save the insurance record to db
        person.addInsurance(insuranceRecord);
        personRepository.save(person);

        //update the personal information
        Person updatedPerson = personRepository.findById(id).get();
        initializeModel(updatedPerson, model);
        return "personDetails";
    }

    private void initializeModel(Person person, Model model) {
        List<Education> sortedEducation = sortEducation(person);
        List<SocialInsuranceRecord> sortedInsuranceRecords = sortInsuranceRecords(person);

        model.addAttribute("person", person);
        model.addAttribute("educations", sortedEducation);
        model.addAttribute("insuranceRecords", sortedInsuranceRecords);
    }

    //check eligibility
    private boolean checkPersonEligibility(Person person) {

        Set<Education> educationSet = person.getEducations();
        Set<SocialInsuranceRecord> socialInsuranceRecords = person.getInsuranceRecords();

        boolean hasSecondaryEducation = false;
        for (Education education : educationSet) {
            if (education instanceof SecondaryEducation && education.hasGraduated()) {
                hasSecondaryEducation = true;
            }
        }
        //check if person has no records for the last three months
        boolean noInsuranceAfterDeadline = true;
        YearMonth cutOffDate = YearMonth.now().minusMonths(MONTHS_BACK_TO_CUT_OFF);
        for (SocialInsuranceRecord record : socialInsuranceRecords) {
            if (record.getYearMonth().isAfter(cutOffDate)) {
                noInsuranceAfterDeadline = false;
                break;
            }
        }

        return hasSecondaryEducation && noInsuranceAfterDeadline;
    }

    //calculate the social aid amount
    private double calcAidAmount(Person person) {
        Set<SocialInsuranceRecord> socialInsuranceRecords = person.getInsuranceRecords();
        YearMonth cutOffDate = YearMonth.now().minusMonths(MONTHS_BACK_TO_COUNT);
        double aidAmount = 0;
        for (SocialInsuranceRecord socialInsuranceRecord : socialInsuranceRecords) {
            if (socialInsuranceRecord.getYearMonth().isAfter(cutOffDate)) {
                aidAmount += socialInsuranceRecord.getAmount();
            }
        }
        return aidAmount / MONTHS_TO_DIVIDE_TO;
    }

    //sort Insurance records by date starting from the most recent
    private List<SocialInsuranceRecord> sortInsuranceRecords(Person person) {
        Set<SocialInsuranceRecord> socialInsuranceRecords = person.getInsuranceRecords();
        List<SocialInsuranceRecord> sortedRecords = new ArrayList<>(socialInsuranceRecords);
        sortedRecords.sort(Comparator.comparing(SocialInsuranceRecord::getYearMonth).reversed());

        return sortedRecords;
    }

    //sort Educations by the enrollment date starting from the most recent
    private List<Education> sortEducation(Person person) {
        Set<Education> educations = person.getEducations();
        List<Education> sortedEducations = new ArrayList<>(educations);
        sortedEducations.sort(Comparator.comparing(Education::getEnrollmentDate).reversed());
        return sortedEducations;
    }
}
