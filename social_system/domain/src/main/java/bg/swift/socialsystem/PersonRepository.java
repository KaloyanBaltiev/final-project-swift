package bg.swift.socialsystem;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *  Person repo interface
 */

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
    Iterable<Person> findByFirstNameIgnoreCase(String firstName);
    Iterable<Person> findByLastNameIgnoreCase(String lastName);
    Iterable<Person> findByLastNameAndFirstNameAllIgnoreCase(String lastName, String firstName);
}

