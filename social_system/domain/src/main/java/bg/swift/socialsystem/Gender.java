package bg.swift.socialsystem;

/**
 * Gender enumeration
 */
enum Gender {
    MALE("M"),
    FEMALE("F");

    private final String abbreviation;

    Gender(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    static Gender of(String abbreviation) {
        for (Gender gender : Gender.values()) {
            if (gender.abbreviation.equals(abbreviation)) {
                return gender;
            }
        }
        throw new IllegalArgumentException("No gender for abbreviation: " + abbreviation);
    }
}
