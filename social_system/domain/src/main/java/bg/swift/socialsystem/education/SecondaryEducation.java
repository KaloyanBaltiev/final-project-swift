package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

/**
 * Secondary education class
 */

@Entity
public class SecondaryEducation extends GradedEducation {
    public SecondaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                              String graduated, double finalGrade) {
        super(institutionName, enrollmentDate, graduationDate, graduated, finalGrade);
    }

    private SecondaryEducation() {
        // Hibernate use this
    }
}
