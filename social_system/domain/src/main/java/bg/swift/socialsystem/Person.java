package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Person.java contains all the personal information
 */

@Entity
class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private Gender gender;
    private int height;
    private LocalDate birthday;

    // Spring database relations
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Education> educations;
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SocialInsuranceRecord> insuranceRecords;

    Person(String firstName, String lastName, Gender gender, int height, LocalDate birthday) {

        validateName(firstName);
        validateName(lastName);

        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.birthday = birthday;
        this.educations = new HashSet<>();
        this.insuranceRecords = new HashSet<>();
    }
    
    private Person() {
        //Required by Hibernate
    }

    //Add the address too the person
    void addAddress(Address address) {
        this.address = address;
    }

    //Add the educations list to the person
    void addEducation(List<Education> education) {
        if (!education.isEmpty()) {
            this.educations.addAll(education);
        }
    }

    //Add the insurance list to the person
    void addInsurance(List<SocialInsuranceRecord> insuranceRecordList) {
        if (!insuranceRecordList.isEmpty()) {
            this.insuranceRecords.addAll(insuranceRecordList);
        }
    }

    //Overloaded method, adds single record
    void addInsurance(SocialInsuranceRecord record) {
        this.insuranceRecords.add(record);
    }

    private void validateName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Expected non-empty name.");
        }
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public Integer getId() {
        return id;
    }

    public Set<SocialInsuranceRecord> getInsuranceRecords() {
        return insuranceRecords;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public int getHeight() {
        return height;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Address getAddress() {
        return address;
    }
}
