package bg.swift.socialsystem.education;

/**
 * Education enumeration
 */

public enum EducationCode {
    PRIMARY("P"),
    SECONDARY("S"),
    BACHELOR("B"),
    MASTER("M"),
    DOCTORATE("D");

    private final String abbreviation;

    EducationCode(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public static EducationCode of(String abbreviation) {
        for (EducationCode code : EducationCode.values()) {
            if (code.abbreviation.equals(abbreviation)) {
                return code;
            }
        }
        throw new IllegalArgumentException("No education code for abbreviation: " + abbreviation);
    }
}
