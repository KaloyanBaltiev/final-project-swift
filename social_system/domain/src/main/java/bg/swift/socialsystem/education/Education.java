package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

/**
 * Education information abstract storage
 */

@Entity
public abstract class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String institutionName;
    private LocalDate enrollmentDate;
    private LocalDate graduationDate;
    private String graduated;

    public Education(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, String graduated) {

        validateGraduationDate(graduationDate, enrollmentDate);
        validateStringInput(institutionName);
        validateStringInput(graduated);

        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
        this.institutionName = institutionName;
        this.graduated = graduated;
    }

    Education() {
        //Hibernate use this
    }

    private void validateGraduationDate(LocalDate graduationDate, LocalDate enrollmentDate) {
        if (enrollmentDate.isAfter(graduationDate)) {
            throw new IllegalArgumentException("Graduation date must be after enrollment date.");
        }

        if (enrollmentDate.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Enrolment date must be in the past.");
        }
    }

    private void validateStringInput(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Expected non-empty.");
        }
    }

    public boolean hasGraduated() {
        return this.graduated.equals("YES");
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public String getGraduated() {
        return graduated;
    }
}
