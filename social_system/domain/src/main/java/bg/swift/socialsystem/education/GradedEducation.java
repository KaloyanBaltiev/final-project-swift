package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

/**
 * Graded education information abstract storage
 */

@Entity
public abstract class GradedEducation extends Education {

    private double finalGrade;

    public GradedEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, String graduated,
                           double finalGrade) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
        this.finalGrade = finalGrade;
    }

    GradedEducation() {
        //Hibernate use this
    }

    public double getFinalGrade() {
        return finalGrade;
    }
}
