package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

/**
 * Higher education class
 */

@Entity
public class HigherEducation extends GradedEducation {
    public HigherEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                           String graduated, double finalGrade) {
        super(institutionName, enrollmentDate, graduationDate, graduated, finalGrade);
    }

    private HigherEducation() {
        // Hibernate use this
    }
}
