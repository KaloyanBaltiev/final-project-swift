package bg.swift.socialsystem.insurance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.YearMonth;

/**
 *  Social insurance record class
 */

@Entity
public class SocialInsuranceRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private YearMonth yearMonth;
    private double amount;

    public SocialInsuranceRecord( int year, int month, double amount) {
        this.yearMonth = YearMonth.of(year, month);
        this.amount = amount;
    }

    private SocialInsuranceRecord() {
        // Why Hibernate, WHY !!!
    }

    public double getAmount() {
        return amount;
    }

    public YearMonth getYearMonth() {
        return yearMonth;
    }
}
