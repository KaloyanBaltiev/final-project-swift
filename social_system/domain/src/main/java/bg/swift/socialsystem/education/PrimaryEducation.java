package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import java.time.LocalDate;

/**
 * Primary education class
 */

@Entity
public class PrimaryEducation extends Education {
    public PrimaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                            String graduated) {
        super(institutionName, enrollmentDate, graduationDate, graduated);
    }

    private PrimaryEducation() {
        // Hibernate use this
    }
}
