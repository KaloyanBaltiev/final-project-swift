package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.HigherEducation;
import bg.swift.socialsystem.education.PrimaryEducation;
import bg.swift.socialsystem.education.SecondaryEducation;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

import static bg.swift.socialsystem.PersonRegistrationService.*;

class PersonRegistrationServiceTest {

    @Test
    void parse_person0_line() {

        //GIVEN
        String personDetails = "Емилия;Младенова;F;99;2.11.1975;България;София;Триадица;1010;Георги Кондолов;13";

        //WHEN
        Person person = parsePersonLine(personDetails);

        //THEN
        Assertions.assertEquals("Емилия", person.getFirstName());
        Assertions.assertEquals("Младенова", person.getLastName());
        Assertions.assertEquals(Gender.FEMALE, person.getGender());
        Assertions.assertEquals(99, person.getHeight());
        Assertions.assertEquals(LocalDate.of(1975, 11, 2), person.getBirthday());
        Assertions.assertEquals("България", person.getAddress().getCountry());
        Assertions.assertEquals("София", person.getAddress().getCity());
        Assertions.assertEquals("Триадица", person.getAddress().getMunicipality());
        Assertions.assertEquals("1010", person.getAddress().getPostCode());
        Assertions.assertEquals("Георги Кондолов", person.getAddress().getStreetName());
        Assertions.assertEquals("13", person.getAddress().getStreetNumber());

    }

    @Test
    void parse_person1_line() {

        //GIVEN
        String personDetails = "Кало;Бало;M;180;3.12.1987;България;София;Младост3Е;1010;блок.444;15";

        //WHEN
        Person person = parsePersonLine(personDetails);

        //THEN
        Assertions.assertEquals("Кало", person.getFirstName());
        Assertions.assertEquals("Бало", person.getLastName());
        Assertions.assertEquals(Gender.MALE, person.getGender());
        Assertions.assertEquals(180, person.getHeight());
        Assertions.assertEquals(LocalDate.of(1987, 12, 3), person.getBirthday());
        Assertions.assertEquals("България", person.getAddress().getCountry());
        Assertions.assertEquals("София", person.getAddress().getCity());
        Assertions.assertEquals("Младост3Е", person.getAddress().getMunicipality());
        Assertions.assertEquals("1010", person.getAddress().getPostCode());
        Assertions.assertEquals("блок.444", person.getAddress().getStreetName());
        Assertions.assertEquals("15", person.getAddress().getStreetNumber());

    }

    @Test
    void parse_person2_line() {

        //GIVEN
        String personDetails = "Любо;Ганчев;M;175;2.11.1999;България;Варна;Изгрев;1010;Залез;13";

        //WHEN
        Person person = parsePersonLine(personDetails);

        //THEN
        Assertions.assertEquals("Любо", person.getFirstName());
        Assertions.assertEquals("Ганчев", person.getLastName());
        Assertions.assertEquals(Gender.MALE, person.getGender());
        Assertions.assertEquals(175, person.getHeight());
        Assertions.assertEquals(LocalDate.of(1999, 11, 2), person.getBirthday());
        Assertions.assertEquals("България", person.getAddress().getCountry());
        Assertions.assertEquals("Варна", person.getAddress().getCity());
        Assertions.assertEquals("Изгрев", person.getAddress().getMunicipality());
        Assertions.assertEquals("1010", person.getAddress().getPostCode());
        Assertions.assertEquals("Залез", person.getAddress().getStreetName());
        Assertions.assertEquals("13", person.getAddress().getStreetNumber());

    }

    @Test
    void parse_education0() {

        // GIVEN
        String educationDetails = "P;СОУ Гео Милев;28.9.1981;30.6.1985;YES;;" +
                "S;СОУ Гео Милев;23.9.1987;20.5.1992;YES;5.165";

        // WHEN
        List<Education> education = parseEducationLine(educationDetails);

        // THEN
        Assertions.assertEquals(2, education.size());
        Assertions.assertTrue(education.get(0) instanceof PrimaryEducation);
        Assertions.assertEquals("СОУ Гео Милев", education.get(0).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(1981, 9, 28), education.get(0).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(1985, 6, 30), education.get(0).getGraduationDate());
        Assertions.assertEquals("YES", education.get(0).getGraduated());
        Assertions.assertTrue(education.get(0).hasGraduated());

        Assertions.assertTrue(education.get(1) instanceof SecondaryEducation);
        Assertions.assertEquals("СОУ Гео Милев", education.get(1).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(1987, 9, 23), education.get(1).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(1992, 5, 20), education.get(1).getGraduationDate());
        Assertions.assertEquals("YES", education.get(1).getGraduated());
        Assertions.assertEquals(5.165, ((SecondaryEducation) education.get(1)).getFinalGrade(), 0.001);
        Assertions.assertTrue(education.get(1).hasGraduated());
    }

    @Test
    void parse_education1() {

        // GIVEN
        String educationDetails = "P;СОУ Гео Геов;28.9.2006;30.6.2015;YES;;" +
                "S;СОУ Гео Милев;23.9.2015;20.5.2020;NO;";

        // WHEN
        List<Education> education = parseEducationLine(educationDetails);

        // THEN
        Assertions.assertEquals(2, education.size());
        Assertions.assertTrue(education.get(0) instanceof PrimaryEducation);
        Assertions.assertEquals("СОУ Гео Геов", education.get(0).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(2006, 9, 28), education.get(0).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(2015, 6, 30), education.get(0).getGraduationDate());
        Assertions.assertEquals("YES", education.get(0).getGraduated());
        Assertions.assertTrue(education.get(0).hasGraduated());

        Assertions.assertTrue(education.get(1) instanceof SecondaryEducation);
        Assertions.assertEquals("СОУ Гео Милев", education.get(1).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(2015, 9, 23), education.get(1).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(2020, 5, 20), education.get(1).getGraduationDate());
        Assertions.assertEquals("NO", education.get(1).getGraduated());
        Assertions.assertEquals(-1.000, ((SecondaryEducation) education.get(1)).getFinalGrade(), 0.001);
        Assertions.assertFalse(education.get(1).hasGraduated());
    }


    @Test
    void parse_education2() {

        // GIVEN
        String educationDetails = "P;ОУ Васил Левски;28.9.1995;30.6.2002;YES;;" +
                "S;Т Ал.Ст.Попов;23.9.2002;20.5.2006;YES;5.999;" +
                "B;ТУ Силистра;20.9.2006;21.5.2010;YES;5.180";

        // WHEN
        List<Education> education = parseEducationLine(educationDetails);

        // THEN
        Assertions.assertEquals(3, education.size());
        Assertions.assertTrue(education.get(0) instanceof PrimaryEducation);
        Assertions.assertEquals("ОУ Васил Левски", education.get(0).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(1995, 9, 28), education.get(0).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(2002, 6, 30), education.get(0).getGraduationDate());
        Assertions.assertEquals("YES", education.get(0).getGraduated());

        Assertions.assertTrue(education.get(1) instanceof SecondaryEducation);
        Assertions.assertEquals("Т Ал.Ст.Попов", education.get(1).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(2002, 9, 23), education.get(1).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(2006, 5, 20), education.get(1).getGraduationDate());
        Assertions.assertEquals("YES", education.get(1).getGraduated());
        Assertions.assertEquals(5.999, ((SecondaryEducation) education.get(1)).getFinalGrade(), 0.001);

        Assertions.assertTrue(education.get(2) instanceof HigherEducation);
        Assertions.assertEquals("ТУ Силистра", education.get(2).getInstitutionName());
        Assertions.assertEquals(LocalDate.of(2006, 9, 20), education.get(2).getEnrollmentDate());
        Assertions.assertEquals(LocalDate.of(2010, 5, 21), education.get(2).getGraduationDate());
        Assertions.assertEquals("YES", education.get(1).getGraduated());
        Assertions.assertEquals(5.180, ((HigherEducation) education.get(2)).getFinalGrade(), 0.001);
    }

    @Test
    void parse_insurance() {

        // GIVEN
        String insuranceRecordString = "1606.60;2018;10;1060.78;2018;9;779.25;2018;8";

        // WHEN
        List<SocialInsuranceRecord> insuranceRecords = parseInsuranceLine(insuranceRecordString);

        // THEN
        Assertions.assertEquals(3, insuranceRecords.size());
        Assertions.assertEquals(1606.60, insuranceRecords.get(0).getAmount());
        Assertions.assertEquals(YearMonth.of(2018, 10), insuranceRecords.get(0).getYearMonth());
        Assertions.assertEquals(1060.78, insuranceRecords.get(1).getAmount());
        Assertions.assertEquals(YearMonth.of(2018, 9), insuranceRecords.get(1).getYearMonth());
        Assertions.assertEquals(779.25, insuranceRecords.get(2).getAmount());
        Assertions.assertEquals(YearMonth.of(2018, 8), insuranceRecords.get(2).getYearMonth());
    }
}
