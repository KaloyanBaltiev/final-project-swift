package bg.swift.socialsystem;

import bg.swift.socialsystem.education.*;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Implements ApplicationRunner, handles the console input. Takes a number of inputs on the first line. Next 3 lines
 * contain personal information, address, education and insurance records, each peace of info is separated by ";".
 * Than the appropriate objects are instantiated and stored to a data base with the help of the Spring data.
 */

@Service
public class PersonRegistrationService implements ApplicationRunner {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");
    private final PersonRepository personRepository;

    public PersonRegistrationService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    //socialInsurances list
    static List<SocialInsuranceRecord> parseInsuranceLine(String line3) {
        List<SocialInsuranceRecord> insuranceRecordList = new ArrayList<>();
        String[] split = line3.split(";", -1);

        //check if there is any insurance record
        if (split.length < 3) {
            return insuranceRecordList;    //return empty list
        }
        for (int i = 0; i < split.length; i += 3) {
            double amount = Double.parseDouble(split[i].trim());
            int year = Integer.parseInt(split[i + 1].trim());
            int month = Integer.parseInt(split[i + 2].trim());
            SocialInsuranceRecord socialInsuranceRecord = new
                    SocialInsuranceRecord(year, month, amount);
            insuranceRecordList.add(socialInsuranceRecord);
        }
        return insuranceRecordList;
    }

    //educations list
    static List<Education> parseEducationLine(String line2) {
        List<Education> educations = new ArrayList<>();
        String[] split = line2.split(";", -1);
        //check if there is any education
        if (split.length < 6) {
            return educations;    //return empty list
        }

        for (int i = 0; i < split.length; i += 6) {
            EducationCode educationCode = EducationCode.of(split[i].trim());
            String institutionName = split[i + 1].trim();
            LocalDate enrollmentDate = LocalDate.parse(split[i + 2], formatter);
            LocalDate graduationDate = LocalDate.parse(split[i + 3], formatter);
            String graduated = split[i + 4].trim();
            double finalGrade = -1; //default value for final grade. If seen the wrong education code was entered
            // check if the information string has final grade value
            if (!split[i + 5].isEmpty()) {
                finalGrade = Double.parseDouble(split[i + 5].trim());
            }
            Education education = chooseEducation(educationCode, institutionName, enrollmentDate, graduationDate,
                    graduated, finalGrade);
            educations.add(education);
        }

        return educations;
    }

    static Education chooseEducation(EducationCode educationCode, String institutionName, LocalDate enrollmentDate,
                                     LocalDate graduationDate, String graduated, double finalGrade) {
        switch (educationCode) {
            case PRIMARY:
                return new PrimaryEducation(institutionName, enrollmentDate, graduationDate, graduated);
            case SECONDARY:
                return new SecondaryEducation(institutionName, enrollmentDate, graduationDate, graduated, finalGrade);
            case BACHELOR:
            case MASTER:
            case DOCTORATE:
                return new HigherEducation(institutionName, enrollmentDate, graduationDate, graduated, finalGrade);
            default:
                throw new IllegalArgumentException("Not a legal educational code.");
        }
    }

    static Person parsePersonLine(String line) {
        String[] split = line.split(";");

        String[] personDetails = Arrays.copyOfRange(split, 0, 5);
        String[] addressDetails = Arrays.copyOfRange(split, 5, split.length);

        Person person = createNewPerson(personDetails);
        Address address = createNewAddress(addressDetails);
        person.addAddress(address);

        return person;
    }

    static Address createNewAddress(String[] addressDetails) {
        String country = addressDetails[0].trim();
        String city = addressDetails[1].trim();
        String municipality = addressDetails[2].trim();
        String postcode = addressDetails[3].trim();
        String streetName = addressDetails[4].trim();
        String streetNumber = addressDetails[5].trim();

        return new Address(country, city, municipality, postcode, streetName, streetNumber);
    }

    static Person createNewPerson(String[] personDetails) {
        String firstName = personDetails[0].trim();
        String lastName = personDetails[1].trim();
        Gender gender = Gender.of(personDetails[2].trim());
        int height = Integer.parseInt(personDetails[3].trim());
        LocalDate birthday = LocalDate.parse(personDetails[4].trim(), formatter);

        return new Person(firstName, lastName, gender, height, birthday);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine(); // consume next line

        for (int i = 0; i < n; i++) {
            String line1 = scanner.nextLine();
            Person person = parsePersonLine(line1);

            String line2 = scanner.nextLine();
            List<Education> educationList = parseEducationLine(line2);
            person.addEducation(educationList);

            String line3 = scanner.nextLine();
            List<SocialInsuranceRecord> insuranceRecordList = parseInsuranceLine(line3);
            person.addInsurance(insuranceRecordList);
            personRepository.save(person);
        }
    }
}


