package bg.swift.socialsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  The main method of the console app
 */

@SpringBootApplication
public class SocialSystemConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialSystemConsoleApplication.class, args);
    }
}
